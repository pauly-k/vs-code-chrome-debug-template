const c = console;
c.clear();

const root = document.getElementById("root");
const header = document.createElement("h1");
header.textContent = "VSCode debug template";
root.appendChild(header);

const pre = document.createElement("pre");
const code = document.createElement("code");
code.textContent = 
`npm i
npm start  // starts live server on port 9900
>debug: start debugging  //CMD + P`    

pre.appendChild(code);
root.appendChild(pre);
